<?php

/* 
Faire un ajout de personne
1. Dans le HomeController créer une nouvelle route add qui sera sur le chemin "/add-person"
2. Créer un nouveau twig home/add-person.html.twig et le faire extends de la base
3. Dans la route add, créer un formulaire avec le createForm comme on a déjà fait (utiliser le PersonType que symfony nous a généré)
4. Exposer la vue du formulaire dans le render et faire l'affichage du form dans le fichier twig
5. Dans la méthode add du contrôleur, faire le handleRequest et rajouter le petit if submitted/valid 
et faire un dump de la person créée par le formulaire dedans
6. Dans les arguments de la méthode add, rajouter un argument typé ObjectManager
7. En dessous du dump, utiliser la méthode persist de l'argument ObjectManager et lui donner l'instance 
de Person en argument, puis faire la méthode flush de l'ObjectManager, sans argument
8. Faire un submit du formulaire et vérifier si ça a bien ajouté la personne dans la bdd

II. Afficher les personnes
1. Dans la méthode index de notre HomeController, rajouter en argument un PersonRepository
2. Utiliser cet argument pour faire un findAll() et donner au template le retour de cette méthode
3. Dans le home/index.html.twig faire une boucle sur les personnes pour les afficher, 
éventuellement sous forme de cards bootstrap tiens

III. Afficher une personne
1. Dans le HomeController,  créer une nouvelle route vers /person qui aura un paramètre id dans son path 
(et donc un argument dans la méthode)
2. Utiliser cet id avec le PersonRepository pour aller récupérer la Person voulue
3. Créer un fichier de template, le render et lui exposer la personne récupérée. Faire un affichage de la person dans le twig
4. Dans le home/index.html.twig, changer le lien à l'intérieur de la boucle pour le faire pointer vers 
la route qu'on vient de créer

IV. Remove Person
1. Créer une nouvelle route dans le HomeController en vous basant sur la route faite auparavant, 
celle ci sera sur le path /remove/{person}
2. Dans cette route, injecter le ObjectManager et l'utiliser pour faire un remove auquel on donnera à manger 
la personne récupéré par le ParamConverter, suivi d'un flush
3. Faire ensuite un return d'un redirect qui nous renverra sur l'index 
(chose qu'on fait déjà dans la route add à l'intérieur du if si vous voulez un modèle)
4. Dans le template d'affichage d'une personne spécifique, rajouter un lien remove 
qui enverra sur la route remove qu'on vient de faire

V. Modify Person
1. Créer une nouvelle route dans le HomeController qui ressemblera aux deux précédente, 
avec argument Person et tout, mais qui pointera sur /modify/{person}
2. Dans cette route, créer un formulaire exactement comme dans la route add, 
à la seule différence qu'au lieu de lui donner une nouvelle instance de personne, 
on lui donnera l'instance récupérée dans la route par le ParamConverter
3. On peut utiliser le même template que pour le add à priori
Bonus : voir comment on pourrait utiliser une seule route pour l'update et le add

VI. Relation
1. Créer une nouvelle entité car avec le make:entity en lui précisant bien une propriété "person" 
qui aura comme type "relation" avec Person (pas oublier de faire la migration et le migrate)
2. Créer un form avec le make:form pour l'entité Car
3. Créer un nouveau contrôleur CarController (avec la commande qui va bien)
4. Dans ce contrôleur, dans la route qui a été générée qu'on va appeler "/add-car", 
faire créer un formulaire et tout pour ajouter un voiture (comme on a fait pour le add-person)
Jean DemelAujourd'hui à 10:38
5. Dans l'entité Person rajouter une public function __toString() qui fera un return de la propriété name de la classe
6. Dans le template dédié à l'affichage d'une personne spécifique (one-person.html.twig chez moi), 
faire un for sur la propriété cars de la person et faire l'affichage des voitures dans ce for
(comme ça, on aura l'affichage des voitures directement dans la page de la personne)
7. Rajouter aussi un petit lien "Add a Car" dans ce template
*/

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\PersonRepository;
use App\Form\PersonType;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Person;
use Doctrine\ORM\Mapping\Id;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PersonRepository $repo)
    {
        return $this->render('home/index.html.twig', [
            "person" => $repo->findAll(),
            
        ]);
    }

    /**
     * @Route("/add-person", name="add")
     */
    public function add(Request $request, PersonRepository $repo, ObjectManager $object) {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $object->persist($person);
            $object->flush();
        }

        return $this->render("home/add-person.html.twig", [
            "form" => $form->createView(),
            "person" => $person
        ]);
    }

    /**
     * @Route("/person/{id}", name="find_person")   
     */
    public function find(int $id, PersonRepository $repo) {
        return $this->render("home/find-person.html.twig", [
            "person" => $repo->find($id),
            "cars" => $repo->find($id),
        ]);
    }

    /**
     * @Route("/remove/{person}", name="remove_person")   
     */
    public function remove(Person $person, ObjectManager $manager) {
        $manager->remove($person);
        $manager->flush();

        return $this->redirectToRoute("home");
    }

    /**
     * @Route("modify/{person}", name="modify_person")   
     */
    public function modify(Person $person, ObjectManager $manager, Request $request) {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->merge($person);
            $manager->flush();
            return $this->redirectToRoute("home");
        }

        return $this->render("home/add-person.html.twig", [
            "form" => $form->createView()
        ]);
    }
}