<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Car;
use App\Form\CarType;
use App\Repository\CarRepository;
use Doctrine\ORM\Mapping\Id;

class CarController extends AbstractController
{

    /**
     * @Route("/car/index.html.twig", name="car")
     */
    public function index(CarRepository $repo)
    {
        return $this->render('car/index.html.twig', [
            "car" => $repo->findAll(),

        ]);
    }

    /**
     * @Route("/add-car", name="add_car")
     */
    public function addCar(Request $request, ObjectManager $objectManager)
    {
        $car = new Car();
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $objectManager->persist($car);
            $objectManager->flush();

            return $this->redirectToRoute('home');

        }

        return $this->render('car/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/remove/{car}", name="remove_car")   
     */
    public function remove(Car $car, ObjectManager $manager) {
        $manager->remove($car);
        $manager->flush();

        return $this->redirectToRoute("home");
    }
}