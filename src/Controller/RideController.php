<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Ride;
use App\Form\RideType;
use Doctrine\ORM\Mapping\Id;

class RideController extends AbstractController
{
    /**
     * @Route("/ride", name="ride")
     */
    public function index()
    {
        return $this->render('ride/index.html.twig', [
            'controller_name' => 'RideController',
        ]);
    }

    /**
     * @Route("/add-ride", name="add_ride")
     */
    public function addRide(Request $request, ObjectManager $objectManager)
    {
        $ride = new Ride();
        $form = $this->createForm(RideType::class, $ride);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $objectManager->persist($ride);
            $objectManager->flush();

            return $this->redirectToRoute('home');

        }

        return $this->render('car/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
