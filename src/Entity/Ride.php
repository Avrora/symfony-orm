<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RideRepository")
 */
class Ride
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $distance;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $passengerNb;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $rideTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Car", inversedBy="rides")
     */
    private $car;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(?float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPassengerNb(): ?int
    {
        return $this->passengerNb;
    }

    public function setPassengerNb(?int $passengerNb): self
    {
        $this->passengerNb = $passengerNb;

        return $this;
    }

    public function getRideTime(): ?\DateTimeInterface
    {
        return $this->rideTime;
    }

    public function setRideTime(?\DateTimeInterface $rideTime): self
    {
        $this->rideTime = $rideTime;

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }
}
